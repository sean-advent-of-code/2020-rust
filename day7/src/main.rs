use std::io::{self, BufRead};
use std::collections::HashMap;

struct InsideBag {
    colour: String,
    qty: i32,
}


fn main() {
    let mut input: Vec<String> = Vec::new();
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let line = line.expect("Could not read line from standard in");
        input.push(line);
    }

    let mut bags: HashMap<String, Vec<InsideBag>> = HashMap::new();

    for line in input.iter() {
        bags.insert(line_to_bag_colour(line), line_to_bag_containts(line));
    }

    let mut count = 0;
    for val in bags.values() {
//        println!("{}", val);
        if depth_first_search_for_gold_bag(&val, &bags) {
            count = count + 1;
        }
    }
    println!("count is {}", count);
    match bags.get("shiny gold") {
        Some(inside_bag_contains) => {
            println!("number of bags in shiny gold bag is {}", get_number_of_bags_inside_bag(inside_bag_contains, &bags));
        }
        None => println!("failed to get colour: shiny bag.")
    }
}

fn depth_first_search_for_gold_bag(bag_contains: &Vec<InsideBag>, all_bags: &HashMap<String, Vec<InsideBag>>) -> bool {
    if bag_contains.len() == 0 {
        return false;
    } else {
        for inside_bag in bag_contains.iter() {
            if inside_bag.colour == "shiny gold" {
                return true;
            } else {
                match all_bags.get(&inside_bag.colour) {
                    Some(inside_bag_contains) => {
                        if depth_first_search_for_gold_bag(inside_bag_contains, all_bags) {
                            return true;
                        }
                    }
                    None => println!("failed to get colour: {}.", &inside_bag.colour)
                }
            }
        }
        return false;
    }
}

fn get_number_of_bags_inside_bag(bag_contains: &Vec<InsideBag>, all_bags: &HashMap<String, Vec<InsideBag>>) -> i32 {
    if bag_contains.len() == 0 {
        return 0;
    } else {
        let mut count: i32 = 0;
        for inside_bag in bag_contains.iter() {
            if count == 0 {
                count = inside_bag.qty;
            } else {
                match count.checked_add(inside_bag.qty) {
                    Some(result) => count = result,
                    None => {
                        println!("overflowed adding {} to {}", count, inside_bag.qty);
                        count = 0 // count * inside_bag.qty;
                    }
                }
            }

            match all_bags.get(&inside_bag.colour) {
                Some(inside_bag_contains) => {
                    match count.checked_add(get_number_of_bags_inside_bag(inside_bag_contains, all_bags) * inside_bag.qty) {
                        Some(result) => {
                            if result != 0 {
                                count = result;
                            }
                        }
                        None => {
                            println!("overflowed adding1 {} to {}", count, inside_bag.qty);
                            count = 0;//count * get_number_of_bags_inside_bag(inside_bag_contains, all_bags);
                        }
                    }
                }
                None => println!("failed to get colour: {}.", &inside_bag.colour)
            }
        }
        return count;
    }
}

fn line_to_bag_colour(line: &str) -> String {
    let mut bag_containts_split = line.split(" bags");
    return bag_containts_split.next().unwrap().trim().to_string();
}

// this rest on the idea that all bag colours are 2 words....
fn line_to_bag_containts(line: &str) -> Vec<InsideBag> {
    let mut containts: Vec<InsideBag> = Vec::new();
    let mut without_full_stop = line.split("."); //  hack to get rid of full stop but wasnt sure of another way... consider replacing this with line[:line.len()-2]
    let mut bag_containts_split = without_full_stop.next().unwrap().split("contain");
    let _burn = bag_containts_split.next();

//    let bag_contains = bag_containts_split.next().unwrap().split(",");
    let re = regex::Regex::new(r"\s*(\d+) (\S+\s\S+) bags?").unwrap();
    for cap in re.captures_iter(bag_containts_split.next().unwrap()) {
//        println!("colour: {} qty: {} ", &cap[2], &cap[1]);
        containts.push(InsideBag { colour: (&cap[2]).to_string(), qty: (&cap[1]).parse().unwrap() });
    }

    return containts;
}



