use std::io::{self, BufRead};

fn main() {
    let mut numbers: Vec<i32> = Vec::new();
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let line = line.expect("Could not read line from standard in");
//        println!("{}", line);
        numbers.push(line.parse().unwrap());
    }

    for i in 1..numbers.len() {
        for j in 1..numbers.len() {
            for z in 1..numbers.len() {
                if (numbers[i] + numbers[j] + numbers[z] == 2020)
                {
                    println!("{}", numbers[i] * numbers[j] * numbers[z]);
                }
            }
        }
    }

//    let numbers_iter = numbers.iter();
//    let numbers_iter2 = numbers.iter();
//
//    for val in numbers_iter {
//        for val2 in numbers_iter2 {
//            println!("Got: {} {}", val, val2);
//        }
//    }
}
