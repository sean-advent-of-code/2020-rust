use std::io::{self, BufRead};

fn main() {
    let mut  input: Vec<String> = Vec::new();
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let line = line.expect("Could not read line from standard in");
        input.push(line);
    }
    println!("valid test 1 is: {}", valid_password_part2("1-3 a: abcde"));
    println!("invalid test 2 is: {}", valid_password_part2("1-3 b: cdefg"));
    println!("invalid test 3 is: {}", valid_password_part2("2-9 c: ccccccccc"));
    
    let mut erm = input.iter();
    let mut validNum = 0;
    for line in erm {
        if valid_password_part2(line) {
            validNum = validNum + 1;
        }
    }
    println!("number is {}", validNum)
}

fn valid_password_part2(line: &str) -> bool {
    let (min, max, letter, password) = line_to_data(line);
    
    if (password.chars().nth((min-1) as usize).unwrap() == letter) ^ (password.chars().nth((max-1) as usize).unwrap() == letter) {
        return true;
    }
    return false;
}

fn line_to_data(line: &str) -> (i32, i32, char, &str){
    let mut split = line.split_whitespace();
    let mut numbers = split.next().unwrap().split("-");
    
    let min = numbers.next().unwrap().parse().unwrap();
    let max = numbers.next().unwrap().parse().unwrap();
    
    let letter = split.next().unwrap().chars().next().unwrap();
    
    let passsword = split.next().unwrap();
    return (min, max, letter, passsword);
    
}


