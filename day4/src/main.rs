use std::io::{self, BufRead};
use std::collections::HashMap;

fn main() {
    let mut input: Vec<String> = Vec::new();
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let line = line.expect("Could not read line from standard in");
        input.push(line);
    }

    let passports = split_input_into_single_passports(&input);

    let mut count = 0;
    for passport in passports {
        if hacked_validate_passport(&passport) {
            count = count + 1;
        }
    }
    println!("valid passports are {} ", count);
}


fn hacked_validate_passport(passport_line: &str) -> bool {
    let map = passport_line_to_map(passport_line);
    if map.contains_key("byr") &&
        map.contains_key("iyr") &&
        map.contains_key("eyr") &&
        map.contains_key("hgt") &&
        map.contains_key("hcl") &&
        map.contains_key("ecl") &&
        map.contains_key("pid") {
        return validate_passport_values(&map);
    }
    return false;
}

fn validate_passport_values(passport_map: &HashMap<String, String>) -> bool {
    match passport_map.get("byr") {
        Some(byr) => {
            if byr.chars().count() != 4 {
                return false;
            }
            let byrnum: i32 = byr.parse().unwrap();
            if byrnum > 2003 || byrnum < 1920 {
                return false;
            }
        },
        None => return false
    }
    
    match passport_map.get("iyr") {
        Some(iyr) => {
            if iyr.chars().count() != 4 {
                return false;
            }
            let iyrnum: i32 = iyr.parse().unwrap();
            if iyrnum > 2020 || iyrnum < 2010 {
                return false;
            }
        },
        None => return false
    }

    match passport_map.get("eyr") {
        Some(eyr) => {
            if eyr.chars().count() != 4 {
                return false;
            }
            let eyrnum: i32 = eyr.parse().unwrap();
            if eyrnum > 2030 || eyrnum < 2020 {
                return false;
            }
        },
        None => return false
    }

    match passport_map.get("hgt") {
        Some(hgt) => {
            if hgt.chars().count() < 4 || hgt.chars().count() > 5 {
                return false;
            }
            let (hgtstrnum, unit) = hgt.split_at(hgt.chars().count() - 2);
//            println!("unit {}, height {}", unit, hgtstrnum);
            if unit == "in" {
                let hgtnum: i32 = hgtstrnum.parse().unwrap();
                if hgtnum > 76 || hgtnum < 59 {
                    return false;
                }
            }else {
                let hgtnum: i32 = hgtstrnum.parse().unwrap();
                if hgtnum > 193 || hgtnum < 150 {
                    return false;
                }
            }
           
        },
        None => return false
    }

    match passport_map.get("hcl") {
        Some(hcl) => {
            let re = regex::Regex::new(r"^#[0-9a-f]{6}$").unwrap();
//               println!("hcl: {}, valid: {}", hcl, re.is_match(hcl));
           if !re.is_match(hcl){
               return false;
           }
        },
        None => return false
    }

    match passport_map.get("ecl") {
        Some(ecl) => {
            let re = regex::Regex::new(r"^(amb)|(blu)|(brn)|(gry)|(grn)|(hzl)|(oth)$").unwrap();
//               println!("hcl: {}, valid: {}", hcl, re.is_match(hcl));
            if !re.is_match(ecl){
                return false;
            }
        },
        None => return false
    }
    match passport_map.get("pid") {
        Some(pid) => {
            let re = regex::Regex::new(r"^[0-9]{9}$").unwrap();
//               println!("hcl: {}, valid: {}", pid, re.is_match(pid));
            if !re.is_match(pid){
                return false;
            }
        },
        None => return false
    }
    
    return true;   
}

fn passport_line_to_map(passport_line: &str) -> HashMap<String, String> {
    let mut map: HashMap<String, String> = HashMap::new();
    let split = passport_line.split_whitespace();
    for key_value_string in split {
        let mut key_value_array = key_value_string.split(":");
        map.insert(key_value_array.next().unwrap().to_string().trim().to_string(), key_value_array.next().unwrap().to_string().trim().to_string());
    }
    return map;
}


fn split_input_into_single_passports(input: &Vec<String>) -> Vec<String> {
    let mut passports: Vec<String> = Vec::new();
    let mut previous_break = 0;
    for (i, line) in input.iter().enumerate() {
        if line.is_empty() {
            let mut sb: String = "".to_owned();
            for j in previous_break..i {
                sb.push_str(&input[j]);
                sb.push_str(" ");
            }
            passports.push(sb);


            previous_break = i;
        }
    }
    let mut sb: String = "".to_owned();

    for j in previous_break..input.len() {
        sb.push_str(&input[j]);
        sb.push_str(" ");
    }
    passports.push(sb);

    return passports;
}
