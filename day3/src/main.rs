use std::io::{self, BufRead};

fn main() {
    let mut  input: Vec<String> = Vec::new();
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let line = line.expect("Could not read line from standard in");
        input.push(line);
    }
    
    println!("toboggan will hit {} Trees", (ride_toboggan(&input, 3, 1) *
        ride_toboggan(&input, 1, 1) *
        ride_toboggan(&input, 5, 1) *
        ride_toboggan(&input, 7, 1) *
        ride_toboggan(&input, 1, 2) )
    );
    
}


fn ride_toboggan(forest: &Vec<String>, right: usize, down: usize) -> i64 {
    let mut x_pos  = 0;
    let mut hit_trees = 0;
    for (y_pos, line) in forest.iter().enumerate() {
        if y_pos % down == 0 {
            let x_index = x_pos % line.chars().count();
            if line.chars().nth(x_index as usize).unwrap() == '#' {
                hit_trees = hit_trees +1;
            }
            x_pos = x_pos + right;
        }
    }
    return hit_trees;
}
