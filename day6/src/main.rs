use std::io::{self, BufRead};


fn main() {
    let mut input: Vec<String> = Vec::new();
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let line = line.expect("Could not read line from standard in");
        input.push(line);
    }

    let mut answers: Vec<String> = Vec::new();
    let mut sb: String = "".to_owned();
    let mut first_string = true;
    for line in input.iter() {
        if line.is_empty() {
            answers.push(sb);
            sb = "".to_owned();
            first_string = true;
        } else {
            if first_string {
                sb = line.to_string();
                first_string = false;
            } else {
                let mut temp = "".to_owned();
                for c in line.chars() {
                    for char2 in sb.chars() {
                        if c == char2 {
                            temp.push(c);
                        }
                    }
                }
                sb = temp;
            }
        }
    }
    answers.push(sb);

    let mut total = 0;
    for line in answers {
        total = total + line.chars().count();
    }


    println!("total is {}", total);
}

