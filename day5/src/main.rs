use std::io::{self, BufRead};

fn main() {
    let stdin = io::stdin();
    
    let number_of_bits = 10;
    let highest_id = 2 << (number_of_bits - 1); // 2^10
    let mut max_id = 0;
    let mut min_id = highest_id;
    
    let mut sum = sum_of_integers(highest_id);
    println!("seat ID is: {}", sum);


    for line in stdin.lock().lines() {
        let line = line.expect("Could not read line from standard in");
        let current_id = seat_id_from_line(line);
        if current_id > max_id {
            max_id = current_id;
        }
        if current_id < min_id {
            min_id = current_id;
        }
        sum = sum - current_id;
    }
    sum = sum - sum_of_integers(min_id - 1);
    sum = sum - (sum_of_integers(highest_id) - sum_of_integers(max_id));
    
    println!("seat ID is: {}", sum);
}


fn seat_id_from_line(line: String) -> i32 {
    let mut current_id = 0;
    let (row, column) = line.split_at(7);
    
    for c in row.chars() {
        current_id = current_id << 1;
        if c == 'B' {
             current_id = current_id | 1;
        }
    }
    for c in column.chars() {
        current_id = current_id << 1;
        if c == 'R' {
            current_id = current_id | 1;
        }
    }
    return current_id;
}

fn sum_of_integers(num: i32) -> i32 {
    return (num * (num+1))/2;
}
